# Detecção de colisão do número com o player. É responsável por notificar
# o jogo e o player a respeito da resposta selecionada.
extends Area

func _ready():
	connect("body_enter", self, "on_body_enter");

func on_body_enter(body):
	if body extends preload("res://scripts/pikachu.gd"):
		var selected_number = get_node("sprite").get_frame();
		var game = get_node("/root/game");
		
		if game.get_challenge().get_answer() == selected_number:
			game.on_right_answer();
			body.score_collected();
		else:
			body.pika_hit();
			game.on_wrong_answer();
	
	queue_free();
