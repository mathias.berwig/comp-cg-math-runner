# Responsável por gerar a rua. 
extends Spatial

var pfb_stone;
var pfb_number;

func _ready():
	set_fixed_process(true)

	# Carrega os prefabs de pedra e números
	pfb_stone = load("res://prefabs/stone.tscn");
	pfb_number = load("res://prefabs/numbers.tscn");
	
	# Obtém uma referência para o jogo
	var game = get_node("/root/game");
	
	# Verifica se deve ser gerada uma nova barreira com as respostas do desafio
	if game.should_generate_new_barrier():
		generate_barricade(game.get_challenge());

# Exibe um numero "n" nas posições x, y, z informadas
func generate_number(x, y, z, n):
	var start_pos = Vector3(x, y, z);

	var inst = pfb_number.instance();
	inst.set_translation(start_pos);
	
	var sprite = inst.get_node("sprite");
	sprite.set_frame(n);

	add_child(inst);

# Exibe uma pedra de tamanho randomico nas posições x, y, z informadas
func generate_rock(x, y, z):
	var rock = Vector3(x, y, z);
	
	var inst = pfb_stone.instance();
	inst.add_to_group("obstacle");
	inst.set_translation(rock);
	inst.set_scale(Vector3(1,1,1)*rand_range(0.4, 0.6));
	inst.set_rotation(Vector3(0, deg2rad(rand_range(0, 360)), 0));
	add_child(inst);

# Gera uma barreira com 3 pedras de largura e numeros de resposta para o desafio
func generate_barricade(challenge):
	
	var answer = challenge.get_answer();
	var correct_i = (randi() % 2) - 1;
	
	for i in range(-1, 2):
		generate_rock(i, 0, 0);
		generate_number(i, 1.2, 0, answer if i == correct_i else generate_wrong_answer(answer));

# Gera um numero randomico aleatorio menor que 10 e diferente do n informado		
func generate_wrong_answer(n):
	var rand = randi() % 10;
	return rand if rand != n else generate_wrong_answer(n);
