# Classe que gerencia o ciclo de vida do jogo. Administra a exibição do painel 
# de game over, música, pontuação e inicialização dos desafios.
extends Node

const Challenge = preload("challenge.gd");

var score;

var panel_gameover;
var sample_player;
var player;

func _init():
	randomize();

func _ready():
	# Define a taxa de frames do jogo
	OS.set_target_fps(30.0);
	
	score = 0;
	
	# Ajusta a visibilidade do painel de game over
	panel_gameover = get_node("gui/panel_gameover");
	panel_gameover.hide();
	panel_gameover.get_node("btnRestart").connect("pressed", self, "restart_game");
	panel_gameover.get_node("btnQuit").connect("pressed", self, "quit_game");
	
	# Exibe o desafio na tela
	get_node("gui/lblChallenge").show();
	update_challenge();
	
	# Reproduz a música de background
	sample_player = get_node("music");
	sample_player.play();
	
	# Define a referência do player
	player = get_node("env/pikachu");

###############################################################################
### On Answer
###############################################################################

# Executado quando o jogador seleciona a resposta correta
func on_right_answer():
	score += 1;
	update_score();
	
	challenge = Challenge.new();
	update_challenge();

# Executado quando o jogador seleciona a resposta errada
func on_wrong_answer():
	player.die();
	game_over();

###############################################################################
### Challenge
###############################################################################

var challenge = Challenge.new();
var visible_challenge = null;

var left_distance_for_new_barrier = 3;

func get_challenge():
	return challenge;

# Retorna true quando uma barreira com respostas deve ser gerada no cenário
func should_generate_new_barrier():
	if visible_challenge != challenge:
		if left_distance_for_new_barrier == 0:
			visible_challenge = challenge;
			return true;
		else:
			left_distance_for_new_barrier = left_distance_for_new_barrier - 1;
	return false;

###############################################################################
### UI Updates
###############################################################################

func update_challenge():
	get_node("gui/lblChallenge").set_text(challenge.get_question());

func update_score():
	get_node("gui/lblScore").set_text("Score: " + str(score));

###############################################################################
### Game Lifecycle
###############################################################################

func game_over():
	sample_player.stop();
	panel_gameover.show();

func restart_game():
	get_tree().reload_current_scene();

func quit_game():
	get_tree().quit();
