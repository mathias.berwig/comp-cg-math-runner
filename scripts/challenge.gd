# Classe responsável por gerar um desafio matemático
extends Node

var question;
var answer;

# Define randomicamente uma pergunta e sua resposta entre as cadastradas
func _init():
	var r = randi() % questions.size();
	question = questions[r];
	answer = answers[r];

func get_question():
	return question;
	
func get_answer():
	return answer;

# Arrays com as perguntas e respectivas respostas
const questions = ["1+1", "2+2", "4*2", "3*3", "3^2", "16/4", "?*3=15", "3+3", "5-2", "2^2", "1^0", "√25", "150%5", "√49", "14/2", "5+2-4"];
const answers   = [2, 4, 8, 9, 9, 4, 5, 6, 3, 4, 1, 5, 0, 7, 7, 3];